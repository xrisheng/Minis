#pragma once

#include <mutex>
#include <condition_variable>
#include <deque>
#include <utility>
//some useful reference
// https://github.com/chenshuo/muduo/blob/master/muduo/base
// https://blog.csdn.net/cywosp/article/details/9157379
// https://blog.csdn.net/big_yellow_duck/article/details/52601543
namespace mini {

    template<typename T>
    class MiniBlockQueue {
    public:
        explicit MiniBlockQueue()
                : mutex_(),
                  notEmpty_(),
                  queue_(std::deque<T>(0)) {};

        MiniBlockQueue(const MiniBlockQueue &) = delete;

        MiniBlockQueue &operator=(const MiniBlockQueue &) = delete;

        ~MiniBlockQueue() = default;

        void put(const T &obj) {

            std::unique_lock<std::mutex> lock(mutex_);
            queue_.push_back(obj);
            lock.unlock();
            notEmpty_.notify_all();
        }

        void put(T &&obj) {

            std::unique_lock<std::mutex> lock(mutex_);
            queue_.push_back(std::move(obj));
            lock.unlock();
            notEmpty_.notify_all();
        }

        T take() {
            std::unique_lock<std::mutex> lock(mutex_);
            notEmpty_.wait(lock, [this] {
                return !(this->queue_.empty());
            });

            assert(!queue_.empty());

            T front(std::move(queue_.front()));
            queue_.pop_front();
            lock.unlock();

            return front;

        }

        size_t size() {
            std::unique_lock<std::mutex> lock(mutex_);
            return queue_.size();
        }

        bool isEmpty() {
            std::unique_lock<std::mutex> lock(mutex_);
            return queue_.empty();
        }

    private:
        std::mutex mutex_;
        std::condition_variable notEmpty_;
        std::deque<T> queue_;
    };


    template<typename T>
    class MiniBoundedBlockQueue {
    public:
        explicit MiniBoundedBlockQueue(size_t maxSize)
                : mutex_(),
                  notFull_(),
                  notEmpty_(),
                  maxSize_(maxSize),
                  queue_(std::deque<T>()) {};

        MiniBoundedBlockQueue(const MiniBoundedBlockQueue &) = delete;

        MiniBoundedBlockQueue &operator=(const MiniBoundedBlockQueue &) = delete;

        ~MiniBoundedBlockQueue() = default;

        bool put(const T &obj) {

            std::unique_lock<std::mutex> lock(mutex_);
            notFull_.wait(lock, [this] { return maxSize_ != this->queue_.size(); });

            queue_.push_back(obj);
            notEmpty_.notify_all();
            return true;
        }

        bool put(T &&obj) {

            std::unique_lock<std::mutex> lock(mutex_);
            notFull_.wait(lock, [this] { return maxSize_ != this->queue_.size(); });

            queue_.push_back(std::move(obj));
            notEmpty_.notify_all();
            return true;
        }

        T take() {
            std::unique_lock<std::mutex> lock(mutex_);
            notEmpty_.wait(lock, [this] { return !this->queue_.empty(); });

            T front(std::move(queue_.front()));
            queue_.pop_front();
            notFull_.notify_all();
            return front;
        }

        size_t size() {
            std::unique_lock<std::mutex> lock(mutex_);
            return queue_.size();
        }

        size_t capaticy() {
            std::unique_lock<std::mutex> lock(mutex_);
            return maxSize_;
        }

        bool isEmpty() {
            std::unique_lock<std::mutex> lock(mutex_);
            return queue_.empty();
        }

        bool isFull() {
            std::unique_lock<std::mutex> lock(mutex_);
            return maxSize_ >= queue_.size();
        }

    private:
        std::mutex mutex_;
        std::condition_variable notFull_;
        std::condition_variable notEmpty_;
        size_t maxSize_;
        std::deque<T> queue_;

    };
}