#ifndef MINIS_MINIVECTOR_H
#define MINIS_MINIVECTOR_H

#include<memory>
namespace mini {
    template<typename T>
    class MiniVector {
    public:
        MiniVector() = default;

        MiniVector(const MiniVector &) = default;

        MiniVector &operator=(const MiniVector &) = delete;

        ~MiniVector() {
            free();
        }

        void push_back(T obj) {
            if (capPtr == endPtr) {
                resize();
            }

            alloc.construct(endPtr, obj);
            endPtr++;
        }

        void pop_back() {
            if (startPtr != nullptr) {
                alloc.destroy(endPtr);
                endPtr--;
            }
        }

        T at(int idx) {
            if (startPtr + idx <= endPtr) {
                return *(startPtr + idx);
            } else {
                return *startPtr;
            }
        }

        int size() {
            if (startPtr == nullptr) {
                return 0;
            } else {
                return (endPtr - startPtr);
            }

        }

        int capacity() {
            if (startPtr == nullptr) {
                return 0;
            } else {
                return (capPtr - startPtr);
            }
        }

    private:
        void resize() {
            int newCap = (capacity() > 0) ? (capacity() << 1) : 1;
            auto newStartPtr = alloc.allocate(newCap);
            auto newEndPtr = std::uninitialized_copy(std::make_move_iterator(startPtr), std::make_move_iterator(endPtr),
                                                     newStartPtr);
            free();
            startPtr = newStartPtr;
            endPtr = newEndPtr;
            capPtr = newStartPtr + newCap;

        };

        void free() {
            if (startPtr != nullptr) {
                for (auto iter = startPtr; iter != endPtr; iter++) {
                    alloc.destroy(iter);
                }

                alloc.deallocate(startPtr, endPtr - startPtr);
            }
        };

    private:
        std::allocator<T> alloc;
        T *startPtr;
        T *endPtr;
        T *capPtr;
    };
}
#endif //MINIS_MINIVECTOR_H