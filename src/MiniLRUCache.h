//This is a simplified version of leveldb cache implementation 
//Ref: https://github.com/google/leveldb
#include <stdio.h>

namespace mini {

    // Hash function calculate an hash value for any type of data.
    unsigned int Hash(const char *data, size_t n, unsigned int seed) {
        const unsigned int m = 0xc6a4a793;
        const unsigned int r = 24;
        const char *limit = data + n;
        unsigned int h = seed ^(n * m);

        while (data + 4 <= limit) {
            unsigned int w;
            memcpy(&w, data, sizeof(w));
            data += 4;
            h += w;
            h *= m;
            h ^= (h >> 16);
        }

        switch (limit - data) {
            case 3:
                h += static_cast<unsigned char>(data[2]) << 16;
            case 2:
                h += static_cast<unsigned char>(data[1]) << 8;
            case 1:
                h += static_cast<unsigned char>(data[0]);
                h *= m;
                h ^= (h >> r);
        }
        return h;
    };


    //Slice is used as a wrapper for void* pointer and const char* pointer type of data.
    class MiniSlice {
    public:
        MiniSlice() : data_(""), size_(0) {}

        MiniSlice(const char *c, size_t s) : data_(c), size_(s) {}

        MiniSlice(const char *c) : data_(c), size_(strlen(c)) {}

        const char *data() const { return data_; }

        size_t size() const { return size_; }

        void clear() {
            data_ = "";
            size_ = 0;
        }

        bool operator==(const MiniSlice &y) {
            return (this->size() == y.size()) &&
                   (memcmp(this->data(), y.data(), this->size()) == 0);
        }

        bool operator!=(const MiniSlice &y) {
            return !(*this == y);
        }

    private:
        const char *data_;
        size_t size_;

    };


    //LRUNode is the node in LRU double-linked list
    struct MiniLRUNode {
        void (*deleter)(MiniLRUNode *node);

        MiniLRUNode *prev;
        MiniLRUNode *next;
        MiniLRUNode *next_hash;

        void *value;
        unsigned int hash;
        bool in_cache;

        char key_data[1];
        size_t key_length;

        MiniSlice key() const {
            return MiniSlice(key_data, key_length);
        }
    };


    //MiniHashTable is a look-up table check if one LRUNode already exists
    class MiniHashTable {
    public:
        void Insert(MiniLRUNode* node) {
            MiniLRUNode** ppNode = FindNode(node->key(),node->hash);
            if(ppNode != nullptr){

               node->next_hash =  (*ppNode)->next_hash;
            }
            else{
                elems_++;
            }
            (*ppNode) = node;

            if(elems_>length_){
                Resize();
            }
        }

        void Remove(MiniSlice &key, unsigned int hash) {
            MiniLRUNode** ppNode = FindNode(key,hash);
            if(*ppNode != nullptr){
                //the life cycle of object is maintained out of hash table, no delete here.
                //simple replace the current position with next pointer (could be null) is enough.
                *ppNode = (*ppNode)->next_hash;
                elems_--;
            }
        }

        MiniLRUNode* Lookup(MiniSlice &key, unsigned int hash) {
            return *FindNode(key,hash);
        }

    private:
        MiniLRUNode** FindNode(const MiniSlice &key, unsigned int hash) {
            // ref: https://stackoverflow.com/questions/3072665/bitwise-and-in-place-of-modulus-operator
            // n % 2^i = n & (2^i - 1)
            MiniLRUNode **ppNode = &list_[hash & (length_ - 1)];

            while (*ppNode != nullptr &&
                   ((*ppNode)->hash != hash || (*ppNode)->key() != key)) {

                ppNode = &((*ppNode)->next_hash);
            }
            //the return value of this func must be a pointer to pointer, otherwise the modification in other funcs
            // after FindNode only changes their local copies
            return ppNode;
        }

        void Resize(){
            unsigned int new_length = 2;
            while(new_length < elems_){
                new_length *=2;
            }

            MiniLRUNode** new_list = new MiniLRUNode* [new_length];
            memset(new_list,0, sizeof(MiniLRUNode*)*new_length);

            for(unsigned int i=0;i<length_;i++){
                MiniLRUNode* pNode = list_[i];
                //TODO: why not use if here?
//                if(pNode!= nullptr){
//                    new_list[(pNode)->hash & (new_length-1)] = pNode;
//                }

                while(pNode!= nullptr){
                    MiniLRUNode* next = pNode->next_hash;
                    MiniLRUNode** pptr = &new_list[(pNode)->hash & (new_length-1)];

                    //insert linked list from head
                    pNode->next_hash = *pptr;
                    (*pptr) = pNode;
                    pNode = next;
                }
            }

            delete[] list_;
            list_= new_list;
            length_= new_length;

        }

    private:
        unsigned int length_;
        unsigned int elems_;
        MiniLRUNode **list_;
    };
}