//
// Created by Risheng Xu on 2019/5/7.
//

#ifndef MINIS_MINITHREADPOOL_H
#define MINIS_MINITHREADPOOL_H

#include"MiniBlockQueue.h"
#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <utility>
namespace mini {
// some useful reference
// ref: http://www.cnblogs.com/haippy/p/3284540.html
// ref: https://www.zhihu.com/question/27908489
// ref: https://github.com/progschj/ThreadPool
// ref: https://github.com/lizhenghn123/zl_threadpool/tree/master/ThreadPoolCpp11

//TODO: possible enhancements: task priority, dynamic thread number, task/thread max life cycle, etc...
    class MiniThreadPool {
    public:
        MiniThreadPool(const MiniThreadPool &) = delete;

        MiniThreadPool &operator=(const MiniThreadPool &) = delete;

        explicit MiniThreadPool(size_t threadSize)
                : tasks_(), stop_(false), mutex_(), condition_() {

            for (size_t i = 0; i < threadSize; ++i) {
                workers_.emplace_back(
                        std::thread([this] {
                            for (;;) {
                                std::unique_lock<std::mutex> lock(mutex_);
                                condition_.wait(lock,
                                                [this] {
                                                    return this->stop_ || !this->tasks_.empty();
                                                });

                                if (this->stop_ && this->tasks_.empty()) return;

                                std::function<void()> task = std::move(this->tasks_.front());

                                this->tasks_.pop_front();
                                lock.unlock();
                                task();
                            }
                        })
                );
            }
        }

        ~MiniThreadPool() {
            std::unique_lock<std::mutex> lock(mutex_);
            if (!stop_) { stop_ = true; }
            lock.unlock();
            condition_.notify_one();

            for (std::thread &worker : workers_) {
                worker.join();
            }
        }

        //TODO: The args must have copy construtor, as the std::bind call it
        template<typename Func, typename... Args>
        std::future<typename std::result_of<Func(Args...)>::type>
        runTwoWay(Func &&f, Args &&...args) {

            //difference between async & packed_task
            //ref: https://stackoverflow.com/questions/18143661/what-is-the-difference-between-packaged-task-and-async

            using returnType = typename std::result_of<Func(Args...)>::type;
            using taskType = std::packaged_task<returnType()>;

            std::shared_ptr<taskType> taskPtr = std::make_shared<taskType>(
                    std::bind(std::forward<Func>(f), std::forward<Args>(args)...));

            std::future<returnType> res = taskPtr->get_future();
            std::unique_lock<std::mutex> lock(mutex_);

            if (stop_)
                return res;
            tasks_.push_back(std::move([taskPtr]() {
                (*taskPtr)();

            }));

            lock.unlock();
            condition_.notify_all();
            return res;
        }

        template<typename Func>
        void runOneWay(Func &&f) {

            std::unique_lock<std::mutex> lock(mutex_);

            if (stop_)
                return;
            tasks_.push_back(std::forward<Func>(f));

            lock.unlock();
            condition_.notify_all();
        }

    private:
        std::vector<std::thread> workers_;
        std::deque<std::function<void()>> tasks_;
        bool stop_;
        std::mutex mutex_;
        std::condition_variable condition_;
    };
}
#endif //MINIS_MINITHREADPOOL_H