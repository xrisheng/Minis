//
// Created by Risheng Xu on 2019/5/3.
//

//some useful reference:
//http://qamichaelpeng.github.io/2014/11/20/sharepointer_1.html
//https://blog.csdn.net/K346K346/article/details/81478223
//http://www.cnblogs.com/wxquare/p/4759020.html
//https://www.modernescpp.com/index.php/atomic-smart-pointers

#ifndef MINIS_MINISMARTPTR_H
#define MINIS_MINISMARTPTR_H

#include <stdatomic.h>
//TODO(xrisheng) support array as C++ 20?
namespace mini {
//The reason using a reference count class instead of a integer is for two reasons:
//1. Make it thread safe
//2. the type of original type should be stored in this class against down-casting
//ref: https://stackoverflow.com/questions/20896618/a-simple-implementation-of-smart-pointer-class
    template<class T>
    class RefCount {
    public:

        // delete copy construct, reference count is a "singleton" during the copying of shared pointer, it is independent from
        // the life cycle of shared pointer, and should not be copied.
        RefCount(RefCount &) = delete;

        RefCount<T> &operator=(const RefCount<T> rhs) = delete;

        explicit RefCount()
                : shared_cnt(atomic_long(0)), weak_cnt(atomic_long(0)), original(nullptr) {}

        explicit RefCount(T *ptr)
                : shared_cnt(atomic_long(1)), weak_cnt(atomic_long(0)), original(ptr) {}

        ~RefCount() {
            if (original != nullptr) {
                delete original;
                original = nullptr;
            }
        }

        //cannot return here, if so, the obj in copy constructor is no more const
        void atomicSharedInc() {
            ++shared_cnt;
        }

        void atomicSharedDec() {
            --shared_cnt;
        }

        void atomicWeakInc() {
            ++weak_cnt;
        }

        void atomicWeakDec() {
            --weak_cnt;
        }

        long getSharedCnt() const {
            return long(shared_cnt);
        }

        long getWeakCnt() const {
            return long(weak_cnt);
        }

    private:
        atomic_long shared_cnt;
        atomic_long weak_cnt;       //use atomic long for thread safe counting.
        T *original;      //keep original type for safe deleting.
    };

    template<class T>
    class MiniWeakPtr; //declaim weak ptr first.

    template<class T>
    class MiniSharedPtr {
    public:
        explicit MiniSharedPtr() : raw(nullptr), ref(nullptr) {};

        explicit MiniSharedPtr(T *ptr) : raw(ptr), ref(new RefCount<T>(ptr)) {
        };

        MiniSharedPtr(MiniSharedPtr<T> const &sp) {
            assign(sp);
        };

        //used in MiniWeakPtr lock();
        explicit MiniSharedPtr(MiniWeakPtr<T> const &wp) {
            raw = wp.raw;
            if (raw != nullptr) {
                (wp.ref)->atomicSharedInc();
                ref = wp.ref;
            }
        };

        ~MiniSharedPtr() {
            release();
        };

        T *getRaw() const {
            return raw;
        }

        RefCount<T> *getRef() const {
            return ref;
        }

        void release() {
            if (ref != nullptr) {
                ref->atomicSharedDec();
                if (ref->getSharedCnt() <= 0) {
                    delete ref; //by delete ref, the original type of raw pointer was also deleted.
                    ref = nullptr;
                }
            }
        }

        void assign(MiniSharedPtr<T> const &sp) {
            T *tmp = sp.getRaw();

            if (tmp != nullptr) {
                ref = sp.getRef();
                raw = tmp;
                ref->atomicSharedInc();
            }
        }

        MiniSharedPtr<T> &operator=(MiniSharedPtr<T> const &sp) {
            release();
            assign(sp);
            return *this;
        };

        MiniSharedPtr<T> &operator=(const T *rp) {
            release();

            if (rp != nullptr) {
                ref = new RefCount<T>(rp);
                raw = rp;
            }
            return *this;
        };

        //safe idiom bool deprecated from c++ 11
        //ref: https://stackoverflow.com/questions/6242768/is-the-safe-bool-idiom-obsolete-in-c11
        //ref: https://www.artima.com/cppsource/safebool.html
        explicit operator bool() const {

            return (raw != nullptr);
        };

        T *operator->() {

            return raw;
        };

        T &operator*() {
            return *raw;
        };
    private:
        T *raw;
        //using pointer here, as a shared pointer might have multi copies, and the life cycle of RefCount
        // is independent from single share poiter
        RefCount<T> *ref;

    };

//TODO(xrisheng): how to make thread safe?
    template<typename T>
    class MiniWeakPtr {
    public:

        MiniWeakPtr<T> *operator->() = delete;

        MiniWeakPtr<T> &operator*() = delete;

        explicit MiniWeakPtr()
                : ref(nullptr), raw(nullptr) {}


        explicit MiniWeakPtr(MiniSharedPtr<T> const &sp) {
            raw = sp.getRaw();
            if (raw != nullptr) {
                ref = sp.getRef();
                ref->atomicWeakInc();

            }
        }

        MiniWeakPtr(MiniWeakPtr<T> const &wp) {
            raw = wp.raw;
            if (raw != nullptr) {
                ref = wp.getRef();
                ref->atomicWeakInc();

            }
        }

        ~ MiniWeakPtr() {
            release();
        }

        MiniSharedPtr<T> lock() {

            return MiniSharedPtr<T>(*this);
        }

        void release() {
            if (ref != nullptr) {
                ref->atomicWeakDec();
                if (ref->getWeakCnt() <= 0 && ref->getSharedCnt() <= 0) {
                    ref = nullptr;
                }
            }
        }

        T *getRaw() const {
            return raw;
        }

        RefCount<T> *getRef() const {
            return ref;
        }

        bool isValid() {
            return (ref != nullptr && ref->getSharedCnt() > 0);
        }

        MiniWeakPtr<T> &operator=(MiniWeakPtr<T> const &wp) {
            RefCount<T> *tmp = wp.getRef();

            if (tmp != nullptr) {
                tmp->atomicWeakInc();
            }

            release();

            raw = wp.raw;
            ref = tmp;
        };

        MiniWeakPtr<T> &operator=(MiniSharedPtr<T> const &sp) {
            RefCount<T> *tmp = sp.getRef();
            if (tmp != nullptr) {
                tmp->atomicWeakInc();
            }

            release();

            raw = sp.getRaw();
            ref = tmp;
            return *this;
        }

        friend class MiniSharedPtr<T>;

    private:
        RefCount<T> *ref;
        T *raw;
    };

    template<typename T>
    class MiniUniquePtr {
    public:
        MiniUniquePtr() = delete;

        MiniUniquePtr(const MiniUniquePtr<T> &up) = delete;

        MiniUniquePtr &operator=(const MiniUniquePtr<T> &up) = delete;

        explicit MiniUniquePtr(T *ptr) {
            raw = ptr;
        }

        ~MiniUniquePtr() {
            if (raw != nullptr) {
                delete raw;
                raw = nullptr;
            }
        }

        T *get() {
            return raw;
        }

        void release() {

            if (raw != nullptr) {
                delete raw;
                raw = nullptr;
            }
        }

        void reset(T *ptr) {

            if (raw != nullptr) {
                delete raw;
                raw = nullptr;
            }

            raw = ptr;
        }

        explicit operator bool() const {

            return (raw != nullptr);
        };

        T *operator->() {

            return raw;
        };

        T &operator*() {
            return *raw;
        };

    private:
        T *raw;
    };
}
#endif //MINIS_MINISMARTPTR_H
