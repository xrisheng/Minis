#!/usr/bin/env bash

git clone https://github.com/google/googletest
cd googletest

[ ! -d build ] && mkdir build
cd build

cmake -DBUILD_SHARED_LIBS=ON -Dgtest_build_samples=OFF -G"Unix Makefiles" \
      -DBUILD_GMOCK=ON -DBUILD_SHARED_LIBS=ON -DINSTALL_GTEST=OFF ..

make
