//
// Created by Risheng Xu on 2019/5/5.
//
#include "gtest/gtest.h"

#include "MiniSmartPtr.h"
using namespace mini;
namespace {
  TEST(MiniSmartPtr, SharedPtrTest) {
    {
      //test reference counting
      MiniSharedPtr<int> msp1(new int(99));
      MiniSharedPtr<int> msp4;
      EXPECT_EQ(1, (msp1.getRef())->getSharedCnt());
      EXPECT_EQ(99, *msp1); //test *operator
      {
        MiniSharedPtr<int> msp2(msp1);
        EXPECT_EQ(2, (msp2.getRef())->getSharedCnt());
        {
          MiniSharedPtr<int> msp3(msp1);
          EXPECT_EQ(3, (msp3.getRef())->getSharedCnt());

          msp4 = msp1;
          EXPECT_EQ(4, (msp4.getRef())->getSharedCnt());
          EXPECT_EQ(4, (msp3.getRef())->getSharedCnt());
          EXPECT_EQ(4, (msp2.getRef())->getSharedCnt());
          EXPECT_EQ(4, (msp1.getRef())->getSharedCnt());
        }
        EXPECT_EQ(3, (msp2.getRef())->getSharedCnt());
      }
      EXPECT_EQ(2, (msp1.getRef())->getSharedCnt());
    }

    struct TestStruct {
      int a = 999;
    };
    MiniSharedPtr<TestStruct> msp6(new TestStruct);
    EXPECT_EQ(true, bool(msp6)); //test bool operator
    EXPECT_EQ(999, msp6->a); //test -> operator
  }


  TEST(MiniSmartPtr, WeakPtrTest) {
    MiniWeakPtr<int> mwp1;
    {
      MiniSharedPtr<int> msp1(new int(99));
      mwp1 = msp1;
      EXPECT_EQ(1, (mwp1.getRef())->getWeakCnt());
      {
        MiniWeakPtr<int> mwp2(mwp1);
        EXPECT_EQ(2, (mwp2.getRef())->getWeakCnt());
        {
          MiniWeakPtr<int> mwp3(msp1);
          EXPECT_EQ(3, (mwp3.getRef())->getWeakCnt());
        }
        EXPECT_EQ(2, (mwp2.getRef())->getWeakCnt());
      }
      EXPECT_EQ(1, (mwp1.getRef())->getWeakCnt());

      EXPECT_TRUE(mwp1.isValid());
      MiniSharedPtr<int> msp2(mwp1.lock());
      EXPECT_EQ(99, *msp2); //test *operator

    }
    EXPECT_FALSE(mwp1.isValid());

  }

  TEST(MiniSmartPtr, UnitPtrTest) {
    MiniUniquePtr<int> mup1(new int(99));
    EXPECT_EQ(99, *mup1); //test *operator

    struct TestStruct {
      int a = 999;
    };
    MiniSharedPtr<TestStruct> mup2(new TestStruct);
    EXPECT_EQ(true, bool(mup2)); //test bool operator
    EXPECT_EQ(999, mup2->a); //test -> operator

    mup1.reset(new int(9));
    EXPECT_EQ(9, *mup1); //test *operator

    mup1.release();
    EXPECT_EQ(nullptr, mup1.get());
  }
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
