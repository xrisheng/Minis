#include <iostream>
#include "gtest/gtest.h"

#include "MiniVector.h"
using namespace mini;

namespace{
    TEST(MiniVector, Test1) {
        MiniVector<int> m;
        m.push_back(1);
        m.push_back(2);
        m.push_back(3);
        EXPECT_EQ(3,m.size());
        EXPECT_EQ(4,m.capacity());
        EXPECT_EQ(1,m.at(0));
        m.pop_back();
        EXPECT_EQ(2,m.size());
        EXPECT_EQ(2,m.at(m.size()-1));
    }
}