//
// Created by Risheng Xu on 2019/5/10.
//
#include <future>
#include <iostream>
#include "gtest/gtest.h"

#include "MiniBlockQueue.h"
using namespace mini;
namespace {
  TEST(MiniBlockQueue, TestBasicBlockQueue) {
    MiniBlockQueue<int> q;
    q.put(99);
    q.put(88);
    EXPECT_EQ(2,q.size());
    EXPECT_FALSE(q.isEmpty());
    int res1 = q.take();
    int res2 = q.take();
    EXPECT_EQ(99,res1);
    EXPECT_EQ(88,res2);
  }

  TEST(MiniBlockQueue, TestMultiThreadBlockQueue) {
    MiniBlockQueue<int> q;
    auto t1 = std::async(std::launch::async, [&q]() {
      for (int i = 0; i < 10; ++i) {
        q.put(i);
        usleep(3);
      }
    });

    auto t2 = std::async(std::launch::async, [&q]() {
    	while (q.size()) {
        //std::cout << "MiniBlockQueue t2 take: " <<q.take() << std::endl;
        int x = q.take()+1;
        usleep(11);
    	}
    });

    auto t3 = std::async(std::launch::async, [&q]() {
    	while (q.size()) {
        //std::cout << "MiniBlockQueue t3 take: " <<q.take() << std::endl;
        int y = q.take()+1;
        usleep(7);
    	}
    });

    t1.wait();
    t3.wait();
    t2.wait();

  }


  TEST(MiniBlockQueue, TestBasicBoundedBlockQueue) {
    MiniBoundedBlockQueue<int> bq(1);
    bq.put(99);
    EXPECT_EQ(1,bq.size());
    EXPECT_EQ(1,bq.capaticy());
    EXPECT_TRUE(bq.isFull());
    int res = bq.take();
    EXPECT_EQ(99,res);
  }

  TEST(MiniBlockQueue, TestMultiThreadBoundedBlockQueue) {
    MiniBoundedBlockQueue<int> q(7);
    auto t1 = std::async(std::launch::async, [&q]() {
      for (int i = 0; i < 10; ++i) {
        q.put(i);
        usleep(3);
      }
      return;
    });

    auto t2 = std::async(std::launch::async, [&q]() {
      while (q.size()) {
        int x = q.take();
        usleep(11);
      }
      return;
    });

    auto t3 = std::async(std::launch::async, [&q]() {
      while (q.size()) {
        int y = q.take();
        usleep(7);
      }
      return;
    });

    t1.wait();
    t3.wait();
    t2.wait();
  }

}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
