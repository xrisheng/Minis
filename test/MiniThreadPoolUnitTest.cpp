//
// Created by Risheng Xu on 2019/5/7.
//
#include <future>
#include "gtest/gtest.h"

#include "MiniThreadPool.h"
using namespace mini;
namespace {
  TEST(MiniThreadPool, TestRunTwoWay) {
    MiniThreadPool pool(4);

    auto result1 = pool.runTwoWay(
        [](int answer) {
          std::this_thread::sleep_for(std::chrono::seconds(1));
          return answer; }, 42);

    auto result2 = pool.runTwoWay(
        [](int answer) {
          std::this_thread::sleep_for(std::chrono::seconds(1));
          return answer; }, 36);

    pool.runOneWay(
        [=]() {
          std::this_thread::sleep_for(std::chrono::seconds(1));
          std::cout << 44 << std::endl; });

     pool.runOneWay(
        []() {
          std::this_thread::sleep_for(std::chrono::seconds(1));
          std::cout << 33 << std::endl; });

    std::cout << result1.get() << std::endl;
    std::cout << result2.get() << std::endl;
  }

}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
